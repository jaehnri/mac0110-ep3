/***** MAC0110 - EP3 *****/
  Nome: <SEU NOME>
  NUSP: <SEU NUSP>

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        A probabilidade de um elemento da matriz ser grama ou terreno é 65%. Notemos que as probabilidades para coelho, lobo e comida, somadas, dão 35%. Caso a probabilidade seja maior que 35%, será terreno ou terreno especial. Portanto, 100 - 35 = 65.

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        O terreno especial tem apenas 0.01 x 0.65 = 0.65% chance de aparecer, ao passo que o terreno comum tem 0.99 * 0.65 = 64.35%.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        Um lobo necessita de ao menos 20 de energia para se reproduzir. Um coelho precisa de um mínimo de 12 de energia para tal.

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Isso acontece porque, para que um animal morra, ele já deve ter energia zero. Sendo assim, alteracões na matriz de energia são desnecessárias.

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        Os coelhos tendem a extincão. Além disso, os números de comida e lobos tendem a reduzir também, pois ficam com as quantidades de comida esgotadas.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Uma combinação que leva a ilha a ser dominada por coelho é o aumento da constante REGENERACAO_TERRENO, que faz os coelhos se alimentarem mais e consequentemente
        se reproduzirem mais. Além disso, diminuindo PROBABILIDADE_LOBO, nascem menos lobos, e obviamente os perigos para os coelhos são diminuidos. Por fim, percebe-se que 
        aumentando ENERGIA_LOBO, os lobos ficam vivos por mais tempo, entretanto tem mais dificuldade para se reproduzirem. Essa combinacão, em geral, leva os coelhos ao domínio
        do território.

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        É notável que a reducão tanto de PROBABILIDADE_COMIDA como de ENERGIA_COMIDA afeta bastante os coelhos, por faltar alimento. Essa falta de alimento não só leva a extincão dos 
        mesmos, mas também dos Lobos, que são afetados pela diminuicão dos coelhos. Essa combinacão contribui para a extincão dos animais na ilha.

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A primeira mudança é que a função simula2 recebe um parâmetro booleano adicional chamado imprime, que define se devem ou não ser imprimidas as funções
        imprime_ilha e analisa_ilha. Além disso, é notável que a função simula2 ainda registra dados num DataFrame, dispondo de maneira mais intuitiva e organizada os dados.
    

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        Ela plota dois gráficos, sendo que o primeiro contém as quantidades de coelhos e lobos ao longo das iterações. O segundo gráfico contém as quantidades de comida e energia na ilha, também ao longo das iterações.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        Os animais cada vez mais beiram à extinção. Após 50 iterações, algumas vezes sobram poucos lobos, mas em geral, são pouquíssimos, que morrem após poucas iterações a mais, visto que não há coelhos. Deve-se a isso também o aumento de comida no tablado.

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        À longo prazo, a constante REGENERACAO_TERRENO funciona como limitante da dominacão dos coelhos. Entretanto, a quantidade de energia da ilha reduz, então a populacão de coelhos passa a não aumentar tanto.

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        A diminuicão da constante ENERGIA_COMIDA leva a uma rápida extincão dos animais da ilha. Entretanto, o gráfico revela que a extincão dos animais leva a um aumento nos níveis de energia, resultado da REGENERACAO_TERRENO.
